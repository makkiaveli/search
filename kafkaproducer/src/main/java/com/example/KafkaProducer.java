package com.example;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

    @Value("${kafka.topicName}")
    private String topicName;

    private final KafkaTemplate<Key, String> kafkaTemplate;

    @Autowired
    public KafkaProducer(KafkaTemplate<Key, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(Key key, String date) {
        LOGGER.info("sending key={}, date='{}'", key, date);
        kafkaTemplate.send(new ProducerRecord<>(topicName, key, date));
    }
}
