package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@SpringBootApplication
public class KafkaProducerApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerApplication.class);

    private static final int threadPool = 10;

    private static Executor executors;

    private static volatile AtomicLong index = new AtomicLong();

    public static void main(String[] args) {
        SpringApplication.run(KafkaProducerApplication.class, args);
    }

    @Autowired
    private KafkaProducer producer;

    @Autowired
    private Message message;

    @PostConstruct
    private void init() {
        executors = Executors.newFixedThreadPool(threadPool);
    }

    @Override
    public void run(String... args) {
        Runnable runnableTask = () -> {
            while (true) {
                try {
                    producer.send(message.createKey(index.getAndIncrement()), message.createMessage());
                    TimeUnit.MILLISECONDS.sleep(3000);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        };
        for (int i = 0; i < threadPool; i++) {
            try {
                executors.execute(runnableTask);
                TimeUnit.MICROSECONDS.sleep(300);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

        }
    }
}
