package com.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class KeySerializer implements Serializer<Key> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, Key data) {
        byte[] serializerData = null;
        ObjectMapper object = new ObjectMapper();
        try {
            serializerData = object.writeValueAsString(data).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serializerData;
    }

    @Override
    public void close() {

    }
}
